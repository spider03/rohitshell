/*
 * main.cpp
 *
 *  Created on: 25-Sep-2016
 *      Author: rohit
 */

#include "mainHeader.h"
char **envVar;

string readFunction()
{
	char *buf;
	string line;
	//rl_bind_key('\t', rl_abort); //disable auto-complete
	while ((buf = readline(printTerminalString().c_str())) != NULL)
	{
		if (buf[0] != 0)
		{
			add_history(buf);
			line = buf;
			return line;
		}
	}

	free(buf);

	return 0;
}


int main(int argc, char **argv, char** envp)
{
	envVar=envp;
	initializeShell(envp);
	signal(SIGINT, signal_handler);
	signal(SIGTSTP,signal_handler);
	cout<<endl;
	cout<<"***************************************************************\n";
	cout<<"\t"<<welcomeString<<"\n";
	cout<<"***************************************************************\n";
	cout<<endl;
	//printTerminalString();
	string input;
	while(1)
	{
		/*char tab[200];
		cin.getline(tab, 200);*/
		input = readFunction();
		if (!input.empty())
			execute(input);
		//printTerminalString();
	}
	return 0;
}

void signal_handler(int signo)
{
	if(signo==SIGCHLD)
	{
		fflush(stdin);
		fflush(stdout);
		updateBackgroundProcess();
	}
	else if(signo == SIGINT)
	{
		if(childPid!=0)
		{
			kill(childPid,SIGKILL);
		}
		if(executionPid!=0)
		{
			kill(executionPid,SIGKILL);
		}
		childPid=0;
		executionPid=0;
		cout<<endl;
		return;
	}
	else if (signo == SIGTSTP)
	{
		updateBackgroundProcess();
		if(executionPid!=0 && !userCmd.empty())
		{
			pair<int, string> processPair;
			processPair.first = executionPid;
			processPair.second = userCmd;
			backgroundProcess.push_back(processPair);
			kill(executionPid, SIGTSTP);
			cout << "[" << backgroundProcess.size() << "]+ "<<stopString<<"\t" << userCmd<< endl;
			childPid=0;
			executionPid=0;
			semaphore=1;
			return;
		}
		cout<<endl;
		return;
	}
}
