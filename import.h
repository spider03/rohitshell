/*
 * import.h
 *
 *  Created on: 26-Sep-2016
 *      Author: rohit
 */

#ifndef IMPORT_H_
#define IMPORT_H_
#define MAX_SIZE 100000

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <string>
#include <map>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include<signal.h>
#include <readline/readline.h>
#include <readline/history.h>

using namespace std;

extern string currentdir;
extern string username;
extern map<string,pair<int,string> > commandMap;
extern vector<string> historyVector;
extern map<string,string > envMap;
extern char **envVar;
extern int childPid;
extern int executionPid;
extern string userCmd;
extern vector<pair<int,string> > backgroundProcess;
extern int semaphore;
extern map<string,string > colorMap;
extern string initDir;

void initializeShell(char** envp);
void loadEnvVariables(char** envp);
void signal_handler(int signo);
string trim(string& str);
string getEnvValue(string variableName);
void fgFunction(vector<string> input);
void loadMap();
void loadLanguages();
void loadLangVariables(string file);
void exitFunction();
void langChangeFunction(vector<string> input);
void jobFunction(vector<string> input);
void loadHistory();
void updateBackgroundProcess();
void writeEnv(string command);
void pwdFunction();
void historyFunction(vector<string> input);
void writeHistory(string command);
string exclamationFunction(string input);
void exportFunction(vector<string> input);
void setCurrentDir();
int checkCommand(string command);
void executeBuiltIn(int commandNo,vector<string> input);
string printTerminalString();
string printVector(vector<vector<string> > commandList);
void execute(string input);
void changeDirectory(vector<string> input);
void clearScreen();
void echoFunction(vector<string> input);
void loadColorMap();

#endif /* IMPORT_H_ */
