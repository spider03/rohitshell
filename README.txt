After using make command it creates a executable file named RohitShell

Supports english and hindi language (lang command)

Install these before running:

sudo apt-get install libreadline5-dev
sudo apt-get install libreadline6-dev


Files Required

builtin.cpp  
english.txt    
hindi.txt    
import.h      
main.cpp      
README.txt
constants.h  
execution.cpp  
language.cpp  
mainHeader.h  
utility.cpp
objects.mk  
sources.mk  
makefile 
subdir.mk