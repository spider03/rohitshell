/*
 * language.cpp
 *
 *  Created on: 01-Oct-2016
 *      Author: rohit
 */

#include "mainHeader.h"

string welcomeString;
string stopString;
string historyErrorMessage;
string endMessage;
string executeErrorMessage;
string noFileDirMessage;
string noCommandMessage;
string newlineErrorMesssage;
string cmdNotAvlMessage;
string noEventMessage;
string manyArgMessage;
string numArgMessage;
string noJobMessage;
string fewArgMessage;

void loadLanguages()
{
	int n;
	cout<<"Select Language\n 1.English\n 2.Hindi"<<endl;
	cout<<">> ";
	cin>>n;
	string file;
	switch(n)
	{
		case 1:	file=initDir+"/english.txt";
				break;
		case 2:	file=initDir+"/hindi.txt";
				break;
	}
	loadLangVariables(file);
}

void loadLangVariables(string file)
{
	ifstream infile(file.c_str());
	string line;
	int count=1;
	while (getline(infile, line))
	{
		switch(count)
		{
			case 1:		welcomeString=line;
						break;
			case 2:		stopString=line;
						break;
			case 3:		historyErrorMessage=line;
						break;
			case 4:		endMessage=line;
						break;
			case 5:		executeErrorMessage=line;
						break;
			case 6:		noFileDirMessage=line;
						break;
			case 7:		noCommandMessage=line;
						break;
			case 8:		newlineErrorMesssage=line;
						break;
			case 9:		cmdNotAvlMessage=line;
						break;
			case 10:	noEventMessage=line;
						break;
			case 11:	manyArgMessage=line;
						break;
			case 12:	numArgMessage=line;
						break;
			case 13:	noJobMessage=line;
						break;
			case 14:	fewArgMessage=line;
						break;

		}
		count++;
	}
}

