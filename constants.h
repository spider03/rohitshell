/*
 * constants.h
 *
 *  Created on: 26-Sep-2016
 *      Author: rohit
 */

#include "import.h"
#ifndef CONSTANTS_H_
#define CONSTANTS_H_


const string builtInString="builtin";
const string linuxString="linux";

//User messages

/*
const string welcomeString="WELCOME TO KALA SHELL";
const string stopString="Stopped";
const string historyErrorMessage="Unable to update history";
const string endMessage="THANKS FOR USING KALA SHELL";
const string executeErrorMessage="Unable to execute";
const string noFileDirMessage="No such file or directory";
const string noCommandMessage="command not found";
const string newlineErrorMesssage="syntax error near unexpected token `newline'";
const string cmdNotAvlMessage="No such command available";
const string noEventMessage="event not found";
const string manyArgMessage="too many arguments";
const string numArgMessage="numeric argument required";
const string noJobMessage="no such job found";
const string fewArgMessage="few arguments";
*/

extern string welcomeString;
extern string stopString;
extern string historyErrorMessage;
extern string endMessage;
extern string executeErrorMessage;
extern string noFileDirMessage;
extern string noCommandMessage;
extern string newlineErrorMesssage;
extern string cmdNotAvlMessage;
extern string noEventMessage;
extern string manyArgMessage;
extern string numArgMessage;
extern string noJobMessage;
extern string fewArgMessage;


#endif /* CONSTANTS_H_ */
