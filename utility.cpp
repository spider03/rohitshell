/*
 * utility.cpp
 *
 *  Created on: 26-Sep-2016
 *      Author: rohit
 */

#include "mainHeader.h"

string currentdir;
string username;
string initDir;

map<string,pair<int,string> > commandMap;
vector<string> builtInList;
vector<string> shellList;
vector<string> historyVector;
map<string,string > envMap;
map<string,string > colorMap;

void initializeShell(char** envp)
{
	setCurrentDir();
	loadMap();
	loadEnvVariables(envp);
	loadColorMap();
	loadLanguages();
}

void loadEnvVariables(char** envp)
{
	envMap.clear();
	char** env;
	for (env = envp; *env != 0; env++)
	{
		char* thisEnv = *env;
		string envString=thisEnv;
		int index=envString.find_first_of('=');
		envMap[envString.substr(0,index)]=envString.substr(index+1,envString.length()-index-1);
	}
	/*ifstream infile("env.txt");
	string line;
	while (getline(infile, line))
	{
		int index=line.find_first_of('=');
		envMap[line.substr(0,index)]=line.substr(index+1,line.length()-index-1);
	}*/
}

void loadHistory()
{
	historyVector.clear();
	string historyFile=initDir+"/history.txt";
	ifstream infile(historyFile.c_str());
	string line;
	while (getline(infile,line))
	{
		historyVector.push_back(line);
		add_history(line.c_str());
	}
}

void updateBackgroundProcess()
{
	vector<pair<int,string> >::iterator it = backgroundProcess.begin();
	while (it != backgroundProcess.end())
	{
		int status;
		pid_t result = waitpid((*it).first, &status, WNOHANG);
		if (result != 0)
		{
			it = backgroundProcess.erase(it);
		}
		else
			++it;
	}
}

void writeHistory(string command)
{
	loadHistory();
	if(historyVector.size()!=0 && (command.compare(historyVector[historyVector.size()-1])==0))
		return;
	string historyFile=initDir+"/history.txt";
	int historyfd=open(historyFile.c_str(),O_APPEND | O_CREAT | O_WRONLY,0777);
	if(historyfd==-1)
	{
		cout<<historyErrorMessage<<endl;
		return;
	}
	char *commandArr=new char[command.length()+1];
	commandArr=strdup(command.c_str());
	commandArr[command.length()]='\n';
	write(historyfd,commandArr,command.length()+1);
	close(historyfd);
}

void loadColorMap()
{
	colorMap["ls"]="ls";
	colorMap["grep"]="grep";
}

void makeBuiltInVector()
{
	builtInList.push_back("cd");
	builtInList.push_back("clear");
	builtInList.push_back("echo");
	builtInList.push_back("pwd");
	builtInList.push_back("history");
	builtInList.push_back("export");
	builtInList.push_back("jobs");
	builtInList.push_back("fg");
	builtInList.push_back("lang");
}

void loadMap()
{
	makeBuiltInVector();
	for (unsigned int i = 0; i < builtInList.size(); i++)
	{
		pair<int, string> commandPair;
		commandPair.first = i + 1;
		commandPair.second = builtInString;
		string key = builtInList[i];
		commandMap[key] = commandPair;
	}
	for (unsigned int i = 0; i < shellList.size(); i++)
	{
		pair<int, string> commandPair;
		commandPair.first = i + 1;
		commandPair.second = linuxString;
		string key = shellList[i];
		commandMap[key] = commandPair;
	}
}

void setCurrentDir()
{
	char cwd[1024];
	if (getcwd(cwd, sizeof(cwd)) != NULL)
	{
		string output=cwd;
		initDir=cwd;
		output=output.substr(6,output.length());
		int index=output.find_first_of('/');
		username=output.substr(0,index);
		currentdir=output.substr(index+1,output.length());
		currentdir="~/"+currentdir;
	}
	else
		perror("getcwd() error");
}

string printTerminalString()
{
	string output="KalaShell:"+currentdir+"$ ";
	//cout<<output;
	return output;
}

void exitFunction()
{
	for(unsigned int j=0;j<backgroundProcess.size();j++)
	{
		int status;
		pid_t result = waitpid(backgroundProcess[j].first, &status, WNOHANG);
		if (result == 0)
		{
			kill(backgroundProcess[j].first,SIGKILL);
		}
	}
	backgroundProcess.clear();
	cout << endl;
	cout<<"***************************************************************\n";
	cout << "\t"<<endMessage<<"\n";
	cout<<"***************************************************************\n";
	cout << endl;
	exit(0);
}


/*string getEnvValue(string variableName)
{
	return NULL;
}*/

/*void writeEnv(string command)
{
	int envfd=open("env.txt",O_APPEND | O_CREAT | O_WRONLY,0777);
	if (envfd == -1)
	{
		cout << "Unable to update history" << endl;
		return;
	}
	char *commandArr = new char[command.length() + 1];
	commandArr = strdup(command.c_str());
	commandArr[command.length()] = '\n';
	write(envfd, commandArr, command.length() + 1);
	close(envfd);
}*/
