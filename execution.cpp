/*
 * execution.cpp
 *
 *  Created on: 26-Sep-2016
 *      Author: rohit
 */

#include "mainHeader.h"
#define READ_END 0
#define WRITE_END 1

int childPid=0;
int executionPid=0;
string userCmd;
vector<pair<int,string> > backgroundProcess;
int semaphore=0;

/*
 * Function for checking commands which do not require fork
 * */
int checkSpecialCommand(vector<vector<string> > commandList)
{
	if(commandList.size()==1)
	{
		if( commandList[0][0]=="cd" )
		{
			executeBuiltIn(commandMap[commandList[0][0]].first,commandList[0]);
			return 1;
		}
		if( commandList[0][0]=="export" )
		{
			executeBuiltIn(commandMap[commandList[0][0]].first, commandList[0]);
			return 1;
		}
		if( commandList[0][0]=="jobs" )
		{
			executeBuiltIn(commandMap[commandList[0][0]].first, commandList[0]);
			return 1;
		}
		if( commandList[0][0]=="fg" )
		{
			executeBuiltIn(commandMap[commandList[0][0]].first, commandList[0]);
			return 1;
		}
		if( commandList[0][0]=="lang" )
		{
			executeBuiltIn(commandMap[commandList[0][0]].first, commandList[0]);
			return 1;
		}
		else if(commandList[0][0]=="exit")
		{
			exitFunction();
		}
	}
	return 0;
}

/*
 * Function for piping multiple commands
 * */
void pipeCommands(vector<vector<string> > commandList,string inputs[],string outputs[])
{
	int fd[2];
	pid_t pid;
	int fd_in = 0;
	int i=0;
	int inputd=0,outputd=0;
	unsigned int k=0;
	while (k<commandList.size())
	{
		pipe(fd);
		if ((pid = fork()) == -1)
		{
			cout<<executeErrorMessage;
			exit(EXIT_FAILURE);
		}
		else if (pid == 0)
		{
			if(!inputs[k].empty())
			{
				if((inputd = open(inputs[k].c_str(),O_RDONLY))==-1)
				{
					cout<<"bash:"<<inputs[k]<<": "<<noFileDirMessage<<endl;
					exit(EXIT_FAILURE);
				}
				dup2(inputd, 0);
			}
			else
				dup2(fd_in, 0);
			if(!outputs[k].empty())
			{
				if((outputd=open(outputs[k].c_str(), O_CREAT | O_WRONLY,0664))==-1)
				{
					cout << "bash:" << outputs[k] << ": "<<noFileDirMessage<< endl;
					exit(EXIT_FAILURE);
				}
				dup2(outputd, 1);
			}
			else if ((k+1)!=commandList.size())
				dup2(fd[WRITE_END], 1);
			close (fd[READ_END]);
			vector<string> commandVector=commandList[k];
			if(checkCommand(commandVector[0])==1)
			{
				executeBuiltIn(commandMap[commandVector[0]].first,commandVector);
			}
			else
			{
				if(colorMap.find(commandVector[0]) != colorMap.end())
				{
					commandVector.push_back("--color=auto");
				}
				char *commandArr[commandVector.size()+1];
				for (unsigned int x = 0; x < commandVector.size(); x++)
				{
					if((commandVector[x].at(0)=='"' && commandVector[x].at(commandVector[x].length()-1)=='"') || (commandVector[x].at(0)=='\'' && commandVector[x].at(commandVector[x].length()-1)=='\''))
					{
						commandVector[x]=commandVector[x].substr(1,commandVector[x].length()-2);
					}
					commandArr[x] = strdup(commandVector[x].c_str());
				}
				commandArr[commandVector.size()] = NULL;
				execvp(commandVector[0].c_str(), commandArr);
				cout << commandVector[0] << ": "<<noCommandMessage<< endl;
			}
			exit(EXIT_FAILURE);
		}
		else
		{
			childPid=pid;
			wait(NULL);
			close (fd[WRITE_END]);
			fd_in = fd[READ_END];
			k++;
			i++;
		}
	}
	childPid=0;
}

/*
 * Function for parsing the input based on blank space
 * */
int parseInputString(string inputString,vector<string> &inputVector)
{
	bool flag=false;
	int end=0;
	unsigned int loopEnd=0;
	for(unsigned int i=0;i<inputString.length();i++)
	{
		if(inputString.at(i)==' ' && !flag)
		{
			loopEnd=i;
			string cmd=inputString.substr(end,loopEnd-end);
			inputVector.push_back(cmd);
			end=i+1;
			/*while(inputString.at(i)==' ')
				i++;
			end=i;*/
		}
		else if(inputString.at(i)=='"' || inputString.at(i)=='\'')
		{
			if(i!=0 && inputString.at(i-1)!='\\')
				flag=!flag;
		}
	}
	loopEnd=inputString.length();
	inputVector.push_back(inputString.substr(end,loopEnd-end));
	return 0;
}


/*
 * Function for getting the commandList from the parsed input
 * */
int getCommandList(vector<string> commandVector,vector<vector<string> > &commandList,string inputs[],string outputs[])
{
	unsigned int x=0,prev=0,k=0;
	while (x < commandVector.size())
	{
		if (strcmp("|", commandVector[x].c_str()) == 0)
		{
			vector<string> commandArg;
			for (unsigned int j = prev; j < x; j++)
			{
				if (strcmp(commandVector[j].c_str(), ">") == 0)
				{
					if (j + 1 == x)
					{
						cout << "bash: "<<newlineErrorMesssage<< endl;
						return -1;
					}
					else
						outputs[k] = commandVector[j+1];
					j++;
				}
				else if (strcmp(commandVector[j].c_str(), "<") == 0)
				{
					if (j + 1 == x)
					{
						cout<< "bash: "<<newlineErrorMesssage<< endl;
						return -1;
					}
					else
						inputs[k] = commandVector[j+1];
					j++;
				}
				else
					commandArg.push_back(commandVector[j]);
			}
			prev = x + 1;
			commandList.push_back(commandArg);
			k++;
		}
		x++;
	}
	vector<string> commandArg;
	for (unsigned int j = prev; j < x; j++)
	{
		if (strcmp(commandVector[j].c_str(), ">") == 0)
		{
			if (j + 1 == x)
			{
				cout << "bash: "<<newlineErrorMesssage<< endl;
				return -1;
			}
			else
				outputs[k] = commandVector[j+1];
			j++;
		}
		else if (strcmp(commandVector[j].c_str(), "<") == 0)
		{
			if (j + 1 == x)
			{
				cout << "bash: "<<newlineErrorMesssage<< endl;
				return -1;
			}
			else
				inputs[k] = commandVector[j+1];
			j++;
		}
		else
			commandArg.push_back(commandVector[j]);
	}
	commandList.push_back(commandArg);
	return 0;
}

/*
 * Function executing the command given by user
 * */
void executeCommand(vector<vector<string> > commandList,string inputs[],string outputs[],bool bgFlag,string command)
{
	if(checkSpecialCommand(commandList)==1)
	{
		return;
	}
	userCmd=command;
	int pid;
	if ((pid = fork()) == -1)
	{
		cout << executeErrorMessage;
		exit(EXIT_FAILURE);
	}
	else if (pid == 0)
	{
		pipeCommands(commandList, inputs, outputs);
		exit(0);
	}
	else
	{
		executionPid = pid;
		if (bgFlag)
		{
			updateBackgroundProcess();
			pair<int, string> processPair;
			processPair.first = executionPid;
			processPair.second = command;
			backgroundProcess.push_back(processPair);
			cout << "[" << backgroundProcess.size() << "] " << executionPid<< endl;
			executionPid = 0;
		}
		else
		{
			int status,result;
			while(semaphore==0 && (result = waitpid(executionPid, &status, WNOHANG)==0));
			semaphore=0;
			executionPid = 0;
		}
		userCmd.clear();
	}
}



/*
 * Function for printing the entire command after parsing
 * */
string printVector(vector<vector<string> > commandList,string inputs[],string outputs[])
{
	string command;
	for(unsigned int i=0;i<commandList.size();i++)
	{
		vector<string> temp=commandList[i];
		for(unsigned int j=0;j<temp.size();j++)
		{
			command=command+temp[j]+" ";
		}
		if(!inputs[i].empty())
		{
			command=command+"< "+inputs[i]+" ";
		}
		if(!outputs[i].empty())
		{
			command=command+"> "+outputs[i]+" ";
		}
		if(i+1!=commandList.size())
			command=command+"| ";
	}
	return command;
}


/*
 * Function for trimming the string
 * */
string trim(string& str)
{
    size_t first = str.find_first_not_of(' ');
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last-first+1));
}

/*
 * Function for checking the bang operator
 * */

int checkExclamation(vector<string> &inputVector)
{
	int flag=0;
	unsigned int x=0;
	vector<string> temp;
	while (x < inputVector.size())
	{
		if(inputVector[x].at(0)=='!')
		{
			flag=1;
			string cmd=exclamationFunction(inputVector[x]);
			if(cmd.empty())
				return 2;
			cmd=trim(cmd);
			vector<string> cmdVector;
			parseInputString(cmd,cmdVector);
			for(unsigned int k=0;k<cmdVector.size();k++)
				temp.push_back(cmdVector[k]);
		}
		else
			temp.push_back(inputVector[x]);
		x++;
	}
	inputVector.clear();
	inputVector.swap(temp);
	return flag;
}

/*
 * Main execute function
 * */

void execute(string input)
{
	bool bgFlag=false;
	string inputs[100];
	string outputs[100];
	vector<string> inputVector;
	vector<vector<string> > commandList;
	input=trim(input);
	parseInputString(input,inputVector);
	int exclamation=checkExclamation(inputVector);
	if(strcmp(inputVector[inputVector.size()-1].c_str(),"&")==0)
	{
		bgFlag=true;
		inputVector.pop_back();
	}
	getCommandList(inputVector,commandList,inputs,outputs);
	string command=printVector(commandList,inputs,outputs);
	writeHistory(command);
	if(exclamation==1)
		cout<<command<<endl;
	else if(exclamation==2)
		return;
	executeCommand(commandList,inputs,outputs,bgFlag,command);
}


/*
 * Function for checking whether string present in history map or not
 * */
int checkCommand(string command)
{
	if(commandMap.find(command) != commandMap.end())
		return 1;
	else
		return 0;
}

