################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../builtin.cpp \
../execution.cpp \
../language.cpp \
../main.cpp \
../utility.cpp 

OBJS += \
./builtin.o \
./execution.o \
./language.o \
./main.o \
./utility.o 

CPP_DEPS += \
./builtin.d \
./execution.d \
./language.d \
./main.d \
./utility.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -lreadline -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


