/*
 * builtin.cpp
 *
 *  Created on: 26-Sep-2016
 *      Author: rohit
 */

#include "mainHeader.h"

void executeBuiltIn(int commandNo,vector<string> input)
{
	switch(commandNo)
	{
		case 1:		changeDirectory(input);
					break;
		case 2: 	clearScreen();
					break;
		case 3:		echoFunction(input);
					break;
		case 4:		pwdFunction();
					break;
		case 5:		historyFunction(input);
					break;
		case 6:		exportFunction(input);
					break;
		case 7:		jobFunction(input);
					break;
		case 8: 	fgFunction(input);
					break;
		case 9: 	langChangeFunction(input);
					break;
		default:	cout<<cmdNotAvlMessage<<endl;
	}
}

void changeDirectory(vector<string> input)
{
	if(input.size()<2)
	{
		return;
	}
	string path=input[1];
	string homeFolder="/home/"+username;
	if(strcmp(path.c_str(),"~")==0 || strcmp(path.c_str(),"~/")==0 )
		path=homeFolder;
	int d=chdir(path.c_str());
	if(d==-1)
	{
		cout <<"bash: cd: "<<path<<": "<<noFileDirMessage<<endl;
		return;
	}
	char cwd[1024];
	if (getcwd(cwd, sizeof(cwd)) != NULL)
	{
		string output = cwd;
		if(output.compare(0, homeFolder.length(), homeFolder) != 0)
			currentdir=output;
		else
		{
			output.replace(0,homeFolder.length(),"~");
			currentdir = output;
		}
	}
	else
		perror("getcwd() error");
}

void clearScreen()
{
	cout << "\x1B[2J\x1B[H";
}

void pwdFunction()
{
	if(currentdir.at(0)!='~')
		cout << currentdir<<endl;
	else
	{
		string temp=currentdir.substr(1,currentdir.length()-1);
		temp="/home/"+username+temp;
		cout << temp<<endl;
	}
}

void echoFunction(vector<string> input)
{
	bool optionEFlag=false;
	string line;
	unsigned int i=1;
	if(strcmp("-e",input[i].c_str())==0)
	{
		optionEFlag=true;
		i++;
	}
	for(;i<input.size();i++)
	{
		line=input[i];
		if(line.at(0)=='"')
		{
			line=line.substr(1,line.length()-2);
			if(optionEFlag)
			{

			}
			char temp[MAX_SIZE];
			int k=0;
			for(unsigned int j=0;j<line.length();j++)
			{
				if(line.at(j)=='$')
				{
					string var;
					unsigned int x = j + 1;
					while (x < line.length() && line.at(x) != ' ') {
						x++;
					}
					if(x!=line.length())
						x--;
					var = line.substr(j + 1, x - j);
					j = x;
					loadEnvVariables(envVar);
					if (strcmp(var.c_str(), "$") == 0) {
						int pid = getppid();
						int start = k;
						while (pid != 0) {
							temp[k++] = (pid % 10) + '0';
							pid = pid / 10;
						}
						k--;
						for (int m = start; k - m > 0; m++) {
							char c = temp[m];
							temp[m] = temp[k - m];
							temp[k - m] = c;
						}
						k++;

					} else if (strcmp(var.c_str(), "PWD") == 0) {
						string value = currentdir;
						if (currentdir.at(0) == '~') {
							string temp = currentdir.substr(1,
									currentdir.length() - 1);
							temp = "/home/" + username + temp;
							value = temp;
						}
						for (unsigned int m = 0; m < value.length(); m++)
							temp[k++] = value.at(m);
					} else if (envMap.find(var) != envMap.end()) {
						string value = envMap[var];
						for (unsigned int m = 0; m < value.length(); m++)
							temp[k++] = value.at(m);
					} else
						temp[k++] = ' ';
				}
				else
					temp[k++]=line.at(j);
			}
			temp[k]='\0';
			line=temp;
		}
		else if(line.at(0)=='\'')
		{
			line=line.substr(1,line.length()-2);
		}
		else
		{
			char temp[MAX_SIZE];
			int k=0;
			for(unsigned int j=0;j<line.length();j++)
			{
				if(line.at(j)!='\\')
				{
					if(line.at(j)=='$')
					{
						string var;
							unsigned int x = j + 1;
							while (x < line.length() && line.at(x) != ' ')
							{
								x++;
							}
							var = line.substr(j + 1, x - j);
							j = x;
							if (strcmp(var.c_str(), "$") == 0)
							{
								int pid = getppid();
								int start = k;
								while (pid != 0)
								{
									temp[k++] = (pid % 10) + '0';
									pid = pid / 10;
								}
								k--;
								for (int m = start; k - m > 0; m++)
								{
									char c = temp[m];
									temp[m] = temp[k - m];
									temp[k - m] = c;
								}
								k++;

							}
							else if (strcmp(var.c_str(), "PWD") == 0)
							{
								string value = currentdir;
								if (currentdir.at(0) == '~')
								{
									string temp = currentdir.substr(1, currentdir.length() - 1);
									temp = "/home/" + username + temp;
									value = temp;
								}
								for (unsigned int m = 0; m < value.length(); m++)
									temp[k++] = value.at(m);
							}
							else if (envMap.find(var) != envMap.end())
							{
								string value = envMap[var];
								for (unsigned int m = 0; m < value.length(); m++)
									temp[k++] = value.at(m);
							}
							else
								temp[k++] = ' ';
					}
					else
						temp[k++]=line.at(j);
				}
			}
			temp[k]='\0';
			line=temp;
		}
		cout<<line<<" ";
	}
	cout<<endl;
}

string exclamationFunction(string input)
{
	loadHistory();
	if(strcmp(input.c_str(),"!!")==0)
		return historyVector[historyVector.size()-1];
	else
	{
		string tp;
		input=input.substr(1,input.length());
		if(input.at(0)=='-')
		{
			input=input.substr(1,input.length());
			if(isdigit(input.at(0)))
			{
				int num = atoi(input.c_str());
				if (1 <= num && num <= 1000)
					return historyVector[historyVector.size() - num];
				else
				{
					cout << "bash: !" << num << ": "<<noEventMessage<< endl;
					return tp;
				}
			}
			return tp;
		}
		else
		{
			for (int i = historyVector.size()-1; i >=0 ; i--)
			{
				string temp=historyVector[i];
				if(temp.at(0)==input.at(0))
				{
					return historyVector[i];
				}
			}
			cout << "bash: !"<<input<<": "<<noEventMessage<< endl;
			return tp;
		}
	}
}

void historyFunction(vector<string> input)
{
	loadHistory();
	if(input.size()>2)
	{
		cout << "bash: history: "<<manyArgMessage<< endl;
		return;
	}
	unsigned int n=0;
	if(input.size()==1)
		n=1000;
	else
	{
		int in=input[1].find_first_not_of( "0123456789");
		if(in!=-1)
		{
			cout<<"bash: history: "<<input[1]<<": "<<numArgMessage<<endl;
			return;
		}
		n=atoi(input[1].c_str());
	}
	unsigned int start=0;
	if (historyVector.size() > n)
		start = historyVector.size() - n;
	for (unsigned int i = start; i < historyVector.size(); i++)
	{
		cout << " " << i + 1000 << "  " << historyVector[i] << endl;
	}
}

void exportFunction(vector<string> input)
{
	for(unsigned int i=1;i<input.size();i++)
	{
		string line=input[i];
		if(line.at(0)=='"' || line.at(0)=='\'')
		{
			line=line.substr(1,line.length()-2);
		}
		int index=line.find_first_of('=');
		if(index!=-1)
		{
			setenv(line.substr(0,index).c_str(),line.substr(index+1,line.length()-index-1).c_str(),1);
			envMap[line.substr(0,index)]=line.substr(index+1,line.length()-index-1);
		}
	}
}

void jobFunction(vector<string> input)
{
	updateBackgroundProcess();
	if(input.size()>1)
	{
		unsigned int num=atoi(input[1].c_str());
		if (num > 0 && num <= backgroundProcess.size())
		{
			cout << "[" << num << "] "<<stopString<<"\t"<< backgroundProcess[num-1].second << endl;
		}
		else
		{
			cout << "bash: jobs: "<<noJobMessage<< endl;
			return;
		}
	}
	else
	{
		for(unsigned int j=0;j<backgroundProcess.size();j++)
		{
			cout << "[" << (j + 1) << "] "<<stopString<<"\t"<< backgroundProcess[j].second << endl;
		}
	}
}

void langChangeFunction(vector<string> input)
{
	if(input.size()>2)
	{
		cout << "bash: lang: " << manyArgMessage << endl;
		return;
	}
	else if (input.size() == 1)
	{
		cout << "bash: lang: " << fewArgMessage << endl;
		return;
	}
	else
	{
		unsigned int num=atoi(input[1].c_str());
		if (num ==1)
		{
			loadLangVariables(initDir+"/english.txt");
		}
		else if (num ==2)
		{
			loadLangVariables(initDir+"/hindi.txt");
		}
		else
		{
			cout << "bash: lang: " << numArgMessage << endl;
			return;
		}
	}
}

void fgFunction(vector<string> input)
{
	if(input.size()>2)
	{
		cout << "bash: fg: "<<manyArgMessage<< endl;
		return;
	}
	else if(input.size()==1)
	{
		cout << "bash: fg: "<<fewArgMessage<< endl;
		return;
	}
	else
	{
		updateBackgroundProcess();
		unsigned int num=atoi(input[1].c_str());
		if(num>0 && num<=backgroundProcess.size())
		{
			cout << backgroundProcess[num-1].second << endl;
			int status,result;
			executionPid=backgroundProcess[num-1].first;
			//cout<<executionPid<<endl;
			kill(executionPid, SIGCONT);
			while(semaphore==0 && (result = waitpid(executionPid, &status, WNOHANG)==0));
				semaphore=0;
		}
		else
		{
			cout << "bash: fg: "<<noJobMessage<< endl;
			return;
		}
	}
}
